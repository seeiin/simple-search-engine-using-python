a simple search engine with python.

crawl data from the web using beautiful soup

clean data using regular expression

weighting using tf-idf

and calculate the similarity of queries with documents using cosine similarity
